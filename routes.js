import React from "react";
import LandingLayouts from "./layouts/LandingLayouts";

import { Navigate, Redirect } from "react-router-dom";
import DCandidates from "./components/DCandidates";

import SidePanel from "./layouts/SidePanel";

import Editable from "./components/Editable";

const routes = [
  {
    path: "app",
    element: <SidePanel />,
    children: [{ path: "dashboard", element: <DCandidates /> }],
  },
  {
    path: "/",
    element: <LandingLayouts />,
    children: [{ path: "/", element: <Editable /> }],
  },
];

export default routes;
