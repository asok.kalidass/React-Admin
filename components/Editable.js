import React, { forwardRef, useState, useEffect } from "react";
import { connect } from "react-redux";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import MaterialTable from "material-table";
import * as actions from "../actions/users";
import { useToasts } from "react-toast-notifications";
import DetailsTwoToneIcon from "@material-ui/icons/DetailsTwoTone";
import Tooltip from "@material-ui/core/Tooltip";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const Editable = ({ classes, ...props }) => {
  //const icons = { tableIcons };

  const columns = [
    {
      title: "Id",
      field: "id",
      hidden: true,
    },
    { title: "Name", field: "fullName" },

    { title: "Age", field: "age", type: "numeric" },
    {
      title: "Email",
      field: "email",
    },
    {
      render: (rowData) =>
        rowData && (
          <Tooltip title="Details" arrow>
            <DetailsTwoToneIcon onClick={() => onDelete(rowData.fullName)} />
          </Tooltip>
        ),
    },
  ];

  useEffect(() => {
    props.fetchAllUsers();
  }, []); //componentDidMount

  //toast msg.
  const { addToast } = useToasts();
  const onDelete = (rowData) => {
    alert("I am clicked " + rowData);
  };

  return (
    <MaterialTable
      icons={tableIcons}
      title="Users Page"
      columns={columns}
      data={props.dCandidateList}
      onRowClick={(event, rowData, togglePanel) =>
        alert("Click on details screen")
      }
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              //setData([...data, newData]);

              resolve();
            }, 1000);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = props.dCandidateList;
              const index = oldData.tableData.id;
              props.dCandidateList[index] = newData;
              props.updateDCandidate(oldData.id, newData, () =>
                addToast("Updated successfully", { appearance: "info" })
              );

              resolve();
            }, 1000);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              //const dataDelete = [...data];
              //const index = oldData.tableData.id;
              //dataDelete.splice(index, 1);
              //setData([...dataDelete]);

              resolve();
            }, 1000);
          }),
      }}
    />
  );
};

const mapStateToProps = (state) => ({
  dCandidateList: state.dCandidate.list,
});

const mapActionToProps = {
  fetchAllUsers: actions.fetchAll,
  updateDCandidate: actions.update,
};

export default connect(mapStateToProps, mapActionToProps)(Editable);
