import React from "react";
import { AppBar, Toolbar, makeStyles } from "@material-ui/core";
import { connect } from "react-redux";
import { StarTwoTone } from "@material-ui/icons";
import clsx from "clsx";

const useStyles = makeStyles({
  root: {},
  toolbar: {
    height: 64,
    background: "#5aa655",
  },
});

const Header = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <AppBar elevation={0} className={clsx(classes.root, className)} {...rest}>
      <Toolbar className={classes.toolbar}></Toolbar>
    </AppBar>
  );
};

//export default connect()(withStyles(useStyles)(Header));
export default Header;
