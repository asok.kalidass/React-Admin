import React from "react";
import Header from "./Header";
import { Outlet } from "react-router-dom";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {},
  wrapper: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
    paddingTop: 64,
  },
  contentContainer: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
  },
  content: {
    flex: "1 1 auto",
    height: "100%",
    overflow: "auto",
  },
});

const LandingLayouts = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Header />
      <div className={classes.contentContainer}>
        <div className={classes.content}></div>
        <div className={classes.wrapper}>
          {/* child components gets injected here */}
          <Outlet />
        </div>
      </div>
    </div>
  );
};

export default LandingLayouts;
