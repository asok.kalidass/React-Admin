import React from "react";
import { NavLink } from "react-router-dom";
import { Button, ListItem, makeStyles } from "@material-ui/core";
import PropTypes from "prop-types";

const DrawerItem = ({ className, href, icon: Icon, title, ...rest }) => {
  return (
    <ListItem>
      <Button component={NavLink} to={href}>
        <Icon />
        <span>{title}</span>
      </Button>
    </ListItem>
  );
};

DrawerItem.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string,
};

export default DrawerItem;
