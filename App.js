import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { store } from "./actions/store";
import { Provider } from "react-redux";
import DCandidates from "./components/DCandidates";
import { Container } from "@material-ui/core";
import { ToastProvider } from "react-toast-notifications";
import Header from "./layouts/LandingLayouts";
import { useRoutes } from "react-router-dom";
import routes from "./routes";
import { ThemeProvider } from "@material-ui/core";
import theme from "./theme";
import GlobalStyles from "./components/GlobalStyles";

function App() {
  const routing = useRoutes(routes);
  return (
    <Provider store={store}>
      <ToastProvider autoDismiss={true}>
        <GlobalStyles />
        <ThemeProvider theme={theme}>{routing}</ThemeProvider>
      </ToastProvider>
    </Provider>
  );
}

export default App;
